(async () => {
  require("dotenv").config();
  const fs = require('fs').promises;
  const ccxt = require("ccxt");
  const exchange = new ccxt.bitmex({
    apiKey: process.env.APIKEY,
    secret: process.env.APISECRET,
  });

  const cacheFile = ".cache.orders.json";
  let cache = [];
  try {
    cache = JSON.parse( await fs.readFile(cacheFile) );
  } catch (error) {
    // cache file was not found or is invalid
    // we will recreate it later 
  }

  try {
    // fetch open orders from BitMEX
    const orders = await exchange.fetchOpenOrders();

    // find open orders in cache which are no longer open
    for (const cacheItem of cache) {
      const found = orders.find((order) => order.id === cacheItem.id);
      if (!found) {
        try {
          order = await exchange.fetchOrder(cacheItem.id);
          console.log("Order to", order.side, order.amount, order.symbol, 
          "for", order.price, "is", order.status);
        } catch (error) {
          console.error("Error fetching individual order data: ", error);
        }
      }
    }

    // use the open orders as the new cache
    cache = orders;

  } catch (error)  {
    console.error(error);
    process.exit(1);
  };

  // save the cache
  try {
    await fs.writeFile(cacheFile, JSON.stringify(cache));
  } catch (error) {
    console.error("Error writing the cache file: ", error);
  }
})();
