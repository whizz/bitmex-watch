# BitMEX watch

Monitor your open orders on BitMEX and get notified when they fill.

## Installation

Clone the repository somewhere, such as `~/bitmex-watcher` and then

```sh
npm install
```

## Configuration

Copy `.env.example` to `.env` and put your BitMEX API key and secret there. Watch-only key is fine.

## order-fill

The script will fetch a list of open orders. It will also load a cached copy from last time it was run. It looks for orders, which are no longer open. If it finds one, it will fetch fresh data for that order and print out the details. If there are no changes in the open orders, nothing is output. This makes the script usable in cron with your notification method of choice.

To run it every minute and send you a notification thru Telegram, put this line in your crontab:

```cron
* * * * * cd ~/bitmex-watch; /usr/bin/node order-fill 2>&1 | telegram-send --stdin
```

## Author

Written by [Michal Taborsky](https://taborsky.cz/about/).
